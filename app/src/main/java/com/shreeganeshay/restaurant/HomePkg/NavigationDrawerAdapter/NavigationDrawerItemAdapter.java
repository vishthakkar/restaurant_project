package com.shreeganeshay.restaurant.HomePkg.NavigationDrawerAdapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.shreeganeshay.restaurant.HomePkg.NavigationDrawerModel.NavigationDrawerItemModel;
import com.shreeganeshay.restaurant.R;

/**
 * Created by Shree Ganeshay on 10/31/2017.
 */

public class NavigationDrawerItemAdapter extends ArrayAdapter<NavigationDrawerItemModel> {

    Context mContext;
    int layoutResourceId;
    NavigationDrawerItemModel data[] = null;

    public NavigationDrawerItemAdapter(Context mContext, int layoutResourceId, NavigationDrawerItemModel[] data) {

        super(mContext, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View listItem = convertView;

        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        listItem = inflater.inflate(layoutResourceId, parent, false);

        ImageView nv_image_view = (ImageView) listItem.findViewById(R.id.nv_image_view);
        TextView nv_text_view_title = (TextView) listItem.findViewById(R.id.nv_text_view_title);

        NavigationDrawerItemModel folder = data[position];


        nv_image_view.setImageResource(R.drawable.ic_menu_camera);
        nv_text_view_title.setText(folder.title);

        return listItem;
    }
}

