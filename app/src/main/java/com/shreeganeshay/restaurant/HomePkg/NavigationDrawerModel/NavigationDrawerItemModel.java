package com.shreeganeshay.restaurant.HomePkg.NavigationDrawerModel;

/**
 * Created by Shree Ganeshay on 10/30/2017.
 */

public class NavigationDrawerItemModel {

    public boolean showNotify;
    public String title;
    public String imageURLPath;


    public NavigationDrawerItemModel() {

    }


    public NavigationDrawerItemModel(
            String title,
            String imageURLPath
    ) {
        this.title = title;
        this.imageURLPath = imageURLPath;
    }
}